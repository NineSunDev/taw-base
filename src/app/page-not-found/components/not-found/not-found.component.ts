import { Component, OnInit } from '@angular/core';
import {PageService} from "../../../shared/services/page/page.service";

@Component({
  selector: 'not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit {
  window: any = window;

  constructor(
    private pageService: PageService
  ) {
    this.pageService.hideHeader();
    this.pageService.hideFooter();
  }

  ngOnInit() {
    this.pageService.setTitle('404');
  }

}
