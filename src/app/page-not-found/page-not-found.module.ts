import {NgModule} from '@angular/core';
import {NotFoundComponent} from "./components/not-found/not-found.component";
import {AngularMaterialModule} from "../angular-material/angular-material.module";
import {RouterModule, Routes} from "@angular/router";

const routes: Routes = [{
    path: '',
    component: NotFoundComponent,
}];

@NgModule({
    declarations: [
        NotFoundComponent
    ],
    imports: [
        // Angular Material
        AngularMaterialModule,
        RouterModule.forChild(routes),
    ],
    exports: [
        RouterModule,
        NotFoundComponent
    ]
})
export class PageNotFoundModule {
}
