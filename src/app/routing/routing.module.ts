import {NgModule} from '@angular/core';

import {
    RouterModule,
    Routes,
} from '@angular/router';

const appRoutes: Routes = [
    {
        path: '',
        redirectTo: '',
        pathMatch: 'full',
    },
    {
        path: 'index',
        loadChildren: '../page-index/page-index.module#PageIndexModule'
    },
    {
        path: 'contact',
        loadChildren: '../page-contact/page-contact.module#PageContactModule'
    },
    {
        path: '**',
        loadChildren: '../page-not-found/page-not-found.module#PageNotFoundModule'
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes, {
            useHash: true,
            enableTracing: false,
            scrollPositionRestoration: 'enabled',
            anchorScrolling: 'enabled',
            urlUpdateStrategy: 'eager',
        }),
    ],
    exports: [
        RouterModule,
    ],
})
export class RoutingModule {
}
