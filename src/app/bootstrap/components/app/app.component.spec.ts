import {TestBed, async} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {PageService} from "../../modules/shared/services/page/page.service";
import {AngularMaterialModule} from "../../modules/angular-material/angular-material.module";
import {RouterTestingModule} from "@angular/router/testing";

describe('AppComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent
            ],
            providers: [
                PageService,
            ],
            imports: [
                AngularMaterialModule,
                RouterTestingModule
            ]
        }).compileComponents();
    }));
    it('should create the app', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));
    it(`should have as title 'app'`, async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app.title).toEqual('App');
    }));
});
