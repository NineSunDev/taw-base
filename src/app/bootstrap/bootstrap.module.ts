import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from "./components/app/app.component";
import {SharedModule} from "../shared/shared.module";
import {RoutingModule} from "../routing/routing.module";

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        RoutingModule,
        SharedModule,
    ],
    bootstrap: [AppComponent]
})
export class BootstrapModule {
}




