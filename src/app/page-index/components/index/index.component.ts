import { Component, OnInit } from '@angular/core';
import {PageService} from "../../../shared/services/page/page.service";

@Component({
  selector: 'index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  constructor(
    private pageService: PageService
  ) { }

  ngOnInit() {
    this.pageService.setTitle('Home');
  }

}
