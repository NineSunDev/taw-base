import {NgModule} from '@angular/core';
import {IndexComponent} from "./components/index/index.component";
import {AngularMaterialModule} from "../angular-material/angular-material.module";
import {RouterModule, Routes} from "@angular/router";

const routes: Routes = [{
    path: '',
    component: IndexComponent,
}];

@NgModule({
    declarations: [
        IndexComponent
    ],
    imports: [
        // Angular Material
        AngularMaterialModule,
        RouterModule.forChild(routes),
    ],
    exports: [
        RouterModule,
        IndexComponent
    ]
})
export class PageIndexModule {
}
