import {TestBed, inject} from '@angular/core/testing';

import {PageService} from './page.service';
import {AngularMaterialModule} from "../../modules/angular-material/angular-material.module";
import {RouterTestingModule} from "@angular/router/testing";


describe('PageService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                PageService,
            ],
            imports: [
                AngularMaterialModule,
                RouterTestingModule
            ]
        });
    });

    it('should be created', inject([PageService], (service: PageService) => {
        expect(service).toBeTruthy();
    }));
});
