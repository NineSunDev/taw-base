import {NgModule} from '@angular/core';
import {ContactComponent} from "./components/contact/contact.component";
import {AngularMaterialModule} from "../angular-material/angular-material.module";
import {RouterModule, Routes} from "@angular/router";

const routes: Routes = [{
    path: '',
    component: ContactComponent,
}];

@NgModule({
    declarations: [
        ContactComponent
    ],
    imports: [
        // Angular Material
        AngularMaterialModule,
        RouterModule.forChild(routes),
    ],
    exports: [
        RouterModule,
        ContactComponent
    ]
})
export class PageContactModule {
}
