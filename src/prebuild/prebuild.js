const fs         = require('fs');
const path       = require('path');
const colors     = require('colors/safe');
const appVersion = require('../../appversion.json');
const appPackage = require('../../package.json');

const packageWhitelist = [
  'name',
  'license',
  'dependencies',
  'devDependencies',
];

const versionWhitelist = [
  'version',
  'status',
  'build',
  'commit',
];

console.log(colors.cyan('\n> Running pre-build tasks'));

// Version Update and Injection
const versionFilePath = path.join(__dirname + '/../environments/app-version.ts');
let appVersionData    = {};

for (let key in appVersion) {
  if (appVersion.hasOwnProperty(key) && versionWhitelist.indexOf(key) !== -1) {
    appVersionData[key] = appVersion[key];
  }
}

fs.writeFile(
  versionFilePath,
  `export const AppVersion = ${JSON.stringify(appVersionData, null, 2)};`,
  {encoding : 'utf8', flag : 'w'}, (err) => {

    if (err) return console.log(colors.red(err));
    console.log(colors.cyan(`Updating version to ${colors.green(
      appVersion.version.major + '.' + appVersion.version.minor + '.' + appVersion.version.patch + ':' +
      appVersion.build.number
    )}`));
    console.log(`${colors.cyan('Writing AppVersion to ')}${colors.green(versionFilePath)}`);
  });


// Version Package and Injection
const depFilePath  = path.join(__dirname + '/../environments/app-package.ts');
let appPackageData = {};

for (let key in appPackage) {
  if (appPackage.hasOwnProperty(key) && packageWhitelist.indexOf(key) !== -1) {
    appPackageData[key] = appPackage[key];
  }
}

fs.writeFile(
  depFilePath,
  `export const AppPackage = ${JSON.stringify(appPackageData, null, 2)};`,
  {encoding : 'utf8', flag : 'w'}, (err) => {
    if (err) return console.log(colors.red(err));
    console.log(`${colors.cyan('Writing AppPackage to ')}${colors.green(depFilePath)}`);
  });
