# TAW
## Version: 2.0.0

A lightweight boilerplate to get started with **T**ypeScript, **A**ngular7 and **W**ebpack

This Boilerplate ships with appversion by default. It is configured in such a way,
that every rebuild increments the build counter for the current version. To bump
a version read below at _Commands_.

This also ships with default pages for index, contact and not-found (404)

If you choose not to use appversion, just delete prebuild/, gulpfile.js and the respecting
scripts in the package.json

The package.json-file and appversion.json-file are accessible through services 
inside your app. Take a look at the footer.component for and example.
See prebuild/prebuild.js for which fields get injected.

Angular Material is available, all used modules can and should be im-/exported via the
src/app/modules/angular-material module.

This boilerplate supports AOT and Lazy Loading out of the box, just put everything in modules :)

Karma test are available but just in a very basic form.

Happy coding. 

### Setup
##### Prerequisites
* node.js v10.x
* npm
* angular/cli v7.x
* appversion

_Of course these also have to be installed globally_


#### Installing
* Get the Repository: `git clone git@bitbucket.org:NineSunDev/taw-base.git`
* Run `npm i` to install it
* Run `npm start` to serve it with watching your sources

#### Recommended Folder-Structure
``` 
    /
    |_ node_modules/
    |_ src/
     |_ app/
      |_ angular-material/
       |_ angular-material.module
      |_ bootstrap/
       |_ bootstrap.module
       |_ components/
         |_ app
          |_ app.component.html
          |_ app.component.scss
          |_ app.component.ts
          |_ app.component.spec.ts
         |_ ...
      |_ routing/
       |_ routing.module
      |_ shared/
       |_ shared.module
       |_ ...
      |_ pages-index/
       |_ pages-index.module
       |_ ...
     |_ assets/
     |_ environments/
     |_ prebuild/
     |_ stylesheets/
     |_ index.html/
     |_ main.js/
     |_ test.js/
     |_ tsconfig.app.json/
     |_ tsconfig.spec.json/
     |_ typings.d.ts/
    |_ angular.json
    |_ appversion.json
    |_ karma.conf.js
    |_ package.json
    |_ package-lock.json
    |_ README.md
    |_ tsconfig.json
    |_ tslint.json
```

### Commands
* Production Build: `npm build`
* Serve: `npm start`
* Testing: `npm test`
* Increment Major Version: `npm run major`
* Increment Minor Version: `npm run minor`
* Increment Patch Version: `npm run patch`
* Code Generation: 
    
    * Modules: `ng g m modules/{{module-name}}`
    * Components: `ng g c components/{{component-name}}`
    * Pages: `ng g c pages/{{page-name}}`
    * Services: `ng g s services/{{service-name}}`
    * Directives: `ng g d directives/{{directive-name}}`
    * Pipes: `ng g p pipes/{{pipe-name}}`
    * ...

### Owner
* [Nico S�nger](nico.saenger@gmail.com) (Nico S�nger)

### License
[![cc-by](https://i.creativecommons.org/l/by/4.0/88x31.png)](http://creativecommons.org/licenses/by/4.0/)
